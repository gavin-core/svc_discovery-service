# README #

[![JavaScript Style Guide](https://cdn.rawgit.com/feross/standard/master/badge.svg)](https://github.com/feross/standard)
[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### NPM Installs ###

> `npm install -g scss`
> `npm install -g bower`

### Mongo Setup ###

* Ensure mongo is running (mogod)
* Run the following commands in your terminal

> `mongod`

> `use discovery_service`

> `db.dbs.save({ "_id" : "log-service", "uri" : "mongodb://127.0.0.1:27017/log_service" })`

>`db.dbs.save({ "_id" : "autobot-queue", "uri" : "mongodb://127.0.0.1:27017/queue" })`
