const core = require('node-core')
const web = require('node-web')

const logger = core.logger

module.exports = function (DiscoveryHost, cookieConfig, jwtConfig, crypto) {
  if (!cookieConfig) {
    throw new Error('No cookie config provided')
  }

  if (!cookieConfig.name) {
    throw new Error('No cookie name config provided')
  }

  if (!cookieConfig.domain) {
    throw new Error('No cookie domain config provided')
  }

  if (!cookieConfig.secret) {
    throw new Error('No cookie secret config provided')
  }

  if (!jwtConfig) {
    throw new Error('No jwt config provided')
  }

  if (!jwtConfig.secret) {
    throw new Error('No jwt secret config provided')
  }

  const conf = {
    host: '0.0.0.0',
    port: 5001
  }

  const configCb = app => {
    app.set('crypto', crypto)
    app.set('cookieConfig', cookieConfig)
    app.set('jwtConfig', jwtConfig)

    const routes = require('./routes')(app, DiscoveryHost)

    web.enableCookieParser(app, cookieConfig.secret)
    app.use(routes.account.authorizeRequest)

    web.enableRequestTracking(app)
    web.enableSendExpressResponder(app, true)
    web.enableUserAgent(app)

    app.get('/authorize', routes.account.authorize)
    app.post('/login', routes.account.login)
    app.get('/logout', routes.account.logout)

    app.use(routes.account.slideCookie)

    app.get('/version', routes.home.version)
    app.get('/', routes.home.index)

    app.use((req, res, next) => {
      if (!req.user) {
        return res.sendexpress.unauthorized()
      }

      next()
    })

    app.get('/autostartservicehosts', routes.autoStartServiceHosts.search)
    app.get('/autostartservices', routes.autoStartServices.search)
    app.put('/autostartservices/:id', routes.autoStartServices.update)
    app.get('/dbs', routes.dbs.search)
    app.get('/ping', routes.home.ping)
    app.get('/services', routes.services.search)
    app.delete('/services/:id', routes.services.stop)
    app.get('/users', routes.users.search)
    app.post('/users', routes.users.create)
  }

  const onListeningCb = (err, serverInfo) => {
    if (err) {
      console.log('Error received but not catered for')
      console.error(err)
    }

    logger.debug(`API server listening on ${serverInfo.host}:${serverInfo.port}`)
  }

  web.createServer(conf, configCb, onListeningCb)
}
