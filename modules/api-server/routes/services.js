const db = require('../../../middleware/db')

module.exports = (app, DiscoveryHost) => {
  return {
    search: (req, res) => {
      const filter = {}

      if (+req.data.type) {
        filter.type = +req.data.type
      }

      db.getDb().services.find(filter).sort({ name: 1 }, (err, services) => {
        if (err) {
          return res.sendexpress(err)
        }

        res.sendexpress(null, services)
      })
    },
    stop: (req, res) => {
      const serviceId = req.params.id

      DiscoveryHost.stopService(serviceId, (err) => {
        if (err) {
          return res.sendexpress(err)
        }

        return res.sendexpress.success(`Queued to stop service ${serviceId}`)
      })
    }
  }
}
