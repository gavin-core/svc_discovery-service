const jwt = require('jsonwebtoken')
const mongojs = require('mongojs')
const db = require('../../../middleware/db')

module.exports = (app, DiscoveryHost) => {
  const crypto = app.get('crypto')

  return {
    authorize: (req, res) => {
      const cookieConfig = app.get('cookieConfig')

      if (!req.signedCookies || !req.signedCookies[cookieConfig.name]) {
        return res.sendexpress.unauthorized('Unauthorized')
      }

      db.getDb().users.findOne({ _id: mongojs.ObjectId(req.signedCookies[cookieConfig.name]) }, (err, user) => {
        if (err) {
          return res.sendexpress.err(err)
        }

        if (!user) {
          return res.sendexpress.unauthorized('Unauthorized')
        }

        delete user.password

        const jwtConfig = app.get('jwtConfig')
        const token = jwt.sign(user, jwtConfig.secret, { expiresIn: jwtConfig.ttl * 60 })

        res.sendexpress(null, { token })
      })
    },
    authorizeRequest: (req, res, next) => {
      const jwtConfig = app.get('jwtConfig')
      const token = (req.headers['authorization'] && req.headers['authorization'].replace(/^Bearer\s/gi, '')) || req.query.token

      if (!token) {
        return next()
      }

      jwt.verify(token, jwtConfig.secret, (err, user) => {
        if (err) {
          return res.sendexpress.unauthorized('Failed to authenticate token')
        }

        if (user) {
          req.user = user
        }

        next()
      })
    },
    login: (req, res) => {
      const username = req.data.username
      const password = req.data.password

      if (!username) {
        return res.sendexpress.preconditionFailed(`No 'username' specified`)
      }
      if (!password) {
        return res.sendexpress.preconditionFailed(`No 'password' specified`)
      }

      const hash = crypto.encrypt(password)

      db.getDb().users.findOne({ username, password: hash }, (err, user) => {
        if (err) {
          return res.sendexpress.err('Oops, something went wrong', err.message)
        }

        if (!user) {
          return res.sendexpress.unauthorized('Invalid credentials')
        }

        const cookieConfig = app.get('cookieConfig')

        res.cookie(cookieConfig.name, user._id.toString(), {
          domain: req.headers.host || cookieConfig.domain,
          path: cookieConfig.path,
          expires: new Date(Date.now() + 1000 * 60 * cookieConfig.ttl),
          signed: true
        })

        res.sendexpress.success('Welcome')
      })
    },
    logout: (req, res) => {
      const cookieConfig = app.get('cookieConfig')
      res.clearCookie(cookieConfig.name, { domain: cookieConfig.domain, path: cookieConfig.path })
      res.sendexpress.success('See you soon')
    },
    slideCookie: (req, res, next) => {
      next = next || (() => {})
      const cookieConfig = app.get('cookieConfig')

      if (!req.signedCookies || !req.signedCookies[cookieConfig.name]) {
        return next()
      }

      res.cookie(cookieConfig.name, req.signedCookies[cookieConfig.name], {
        domain: cookieConfig.domain,
        path: cookieConfig.path,
        expires: new Date(Date.now() + 1000 * 60 * cookieConfig.ttl),
        signed: true
      })

      next()
    }
  }
}
