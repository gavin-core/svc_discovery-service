const db = require('../../../middleware/db')

module.exports = (app, DiscoveryHost) => {
  return {
    search: (req, res) => {
      db.getDb().dbs.find().sort({ _id: 1 }, (err, dbs) => {
        if (err) {
          return res.sendexpress(err)
        }

        res.sendexpress(null, dbs)
      })
    }
  }
}
