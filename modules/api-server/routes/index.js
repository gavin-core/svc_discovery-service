module.exports = (app, DiscoveryHost) => {
  return {
    account: require('./account')(app, DiscoveryHost),
    autoStartServiceHosts: require('./auto-start-service-hosts')(app, DiscoveryHost),
    autoStartServices: require('./auto-start-services')(app, DiscoveryHost),
    dbs: require('./dbs')(app, DiscoveryHost),
    home: require('./home')(app, DiscoveryHost),
    services: require('./services')(app, DiscoveryHost),
    users: require('./users')(app, DiscoveryHost)
  }
}
