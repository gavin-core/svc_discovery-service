const db = require('../../../middleware/db')

module.exports = (app, DiscoveryHost) => {
  return {
    search: (req, res) => {
      db.getDb().auto_start_service_hosts.find().sort({ _id: 1 }, (err, autoStartServiceHosts) => {
        if (err) {
          return res.sendexpress(err)
        }

        res.sendexpress(null, autoStartServiceHosts)
      })
    }
  }
}
