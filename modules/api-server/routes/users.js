const db = require('../../../middleware/db')

module.exports = (app, DiscoveryHost) => {
  return {
    search: (req, res) => {
      db.getDb().users.find((err, users) => {
        if (err) {
          return res.sendexpress(err)
        }

        users.forEach(user => {
          delete user.password
        })

        const sortedUsers = users.sort((a, b) => a.username.toLowerCase() > b.username.toLowerCase())

        res.sendexpress(null, sortedUsers)
      })
    },
    create: (req, res) => {
      if (!req.data.username) {
        return res.sendexpress.preconditionFailed('No username found on the request data')
      }

      if (typeof req.data.username !== 'string') {
        return res.sendexpress.preconditionFailed('Username must be of type: string')
      }

      if (!req.data.password) {
        return res.sendexpress.preconditionFailed('No password found on the request data')
      }

      if (typeof req.data.password !== 'string') {
        return res.sendexpress.preconditionFailed('Password must be of type: string')
      }

      if (req.data.password.length < 6) {
        return res.sendexpress.preconditionFailed('Password must be 6 or more characters long')
      }

      const user = {
        username: req.data.username,
        passowrd: req.data.passowrd
      }

      db.getDb().users.count({ username: user.username }, (err, count) => {
        if (err) {
          return res.sendexpress(err)
        }

        if (count) {
          return res.sendexpress.err(`User with username '${user.username}' already exists`)
        }

        db.getDb().users.save(user, (err, user) => {
          if (err) {
            return res.sendexpress(err)
          }

          delete user.password

          return res.sendexpress(null, user)
        })
      })
    }
  }
}
