const mongojs = require('mongojs')
const db = require('../../../middleware/db')

module.exports = (app, DiscoveryHost) => {
  return {
    search: (req, res) => {
      db.getDb().auto_start_services.find().sort({ name: 1 }, (err, autoStartServices) => {
        if (err) {
          return res.sendexpress(err)
        }

        res.sendexpress(null, autoStartServices)
      })
    },
    create: (req, res) => {
      return res.sendexpress.notImplemented('Not yet implemented')
    },
    update: (req, res) => {
      const autoStartServiceId = req.params.id

      db.getDb().auto_start_services.findOne({ _id: mongojs.ObjectId(autoStartServiceId) }, (err, autoStartService) => {
        if (err) {
          return res.sendexpress(err)
        }

        if (!autoStartService) {
          return res.sendexpress.notFound(`No autoStartService with id:${autoStartServiceId} found`)
        }

        autoStartService.min = req.data.min
        autoStartService.max = req.data.max
        autoStartService.enabled = !!req.data.enabled

        db.getDb().auto_start_services.save(autoStartService, err => {
          if (err) {
            return res.sendexpress(err)
          }

          res.sendexpress(null, autoStartService)
        })
      })
    }
  }
}
