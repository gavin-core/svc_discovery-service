const servicePackage = require('../../service-package')

module.exports = (app, DiscoveryHost) => {
  return {
    index: (req, res) => {
      res.send(`<h1>Discovery Api Server: v${servicePackage.version}</h1>`)
    },
    ping: (req, res) => {
      res.sendexpress.success('pong')
    },
    version: (req, res) => {
      res.sendexpress(null, { version: servicePackage.version })
    }
  }
}
