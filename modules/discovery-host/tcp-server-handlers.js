const core = require('node-core')
const middleware = require('../../middleware')
const dataHandlers = require('./data-handlers')

const db = middleware.db
const Version = core.Version

const _registerDiscoveryService = function (socket, data, cb) {
  const _this = this
  cb = cb || (() => {})

  if (!_this.isMaster) {
    cb(new Error('Not a master discovery service'))
    return setTimeout(() => {
      socket.destroy()
    }, 500)
  }

  socket.type = data.type = _this.CLIENT_TYPES.DISCOVERY_SERVICE
  socket.host = data.host
  data.version = new Version(data.version, '0').toString()

  socket.registerHPCs({
    reqTakeoverAsMaster: function (req, res) {
      if (!this.isMaster) {
        return res.sendError('Not a master discovery service')
      }

      if (!process.askedToTerminate || !this.selectNewMaster) {
        return res.sendError('No broadcast sent looking for new Discovery Master')
      }

      this.selectNewMaster(req.data.priority, isNewMaster => {
        if (!isNewMaster) {
          return res.sendError('Not selected to become master')
        }
        res.send()
      })
    },
    serviceRegistered: function (req, res) {
      this.emit('service-registered-with-slave-disco', req.data)
      this.tcpServer.broadcast('SERVICE_REGISTERED', req.data, s => _this !== s)
    },
    serviceDeregistered: function (req, res) {
      this.emit('service-deregistered-with-slave-disco', req.data)
      this.tcpServer.broadcast('SERVICE_DEREGISTERED', req.data, s => _this !== s)
    }
  })

  dataHandlers.services.saveDiscovery(data, (err, discoModel) => {
    if (err) {
      return cb(err)
    }

    socket.serviceInfo = discoModel
    socket.serviceId = discoModel._id

    _this.emit('discovery-registered', data.host)
    _this.tcpServer.broadcast('DISCOVERY_SERVICE_REGISTERED', data.host, s => socket !== s)

    dataHandlers.autoStartServiceHosts.add(discoModel, data.autoStartServiceConfig)

    socket.on('disconnect', function () {
      const serviceId = this.serviceId
      dataHandlers.services.removeDiscovery({ $or: [{ 'discoHost.id': serviceId }, { _id: serviceId }] }, err => {
        if (err) {
          console.log('Error received but not catered for')
          console.error(err)
        }

        // TODO - if err is thrown, we need to find a way to handle it as the socket has already been lost, cannot prevent disconnect
        // if (err)
        // return cb(err);

        dataHandlers.autoStartServiceHosts.remove(serviceId)
        _this.emit('discovery-deregistered', socket.host)
        _this.tcpServer.broadcast('DISCOVERY_SERVICE_DEREGISTERED', socket.host)
      })
    })

    cb(null, discoModel._id)
  })
}

const _registerService = function (socket, data, cb) {
  const _this = this
  cb = cb || (() => {})

  socket.type = data.type = _this.CLIENT_TYPES.SERVICE
  data.version = new Version(data.version, '0').toString()
  data.host = _this.tcpServer.address()
  data.host.serviceId = _this.serviceInfo._id

  dataHandlers.services.saveService(data, function (err, service) {
    if (err) {
      return cb(err)
    }

    socket.serviceId = service._id
    socket.serviceInfo = service

    _this.emit('service-registered', socket.serviceInfo)
    _this.tcpServer.broadcast('SERVICE_REGISTERED', socket.serviceInfo, s => socket !== s)

    socket.on('disconnect', function () {
      dataHandlers.services.removeServices({ _id: this.serviceId.toObjectID() }, err => {
        if (err) {
          console.log('Error received but not catered for')
          console.error(err)
        }

        _this.emit('service-deregistered', socket.serviceInfo)
        _this.tcpServer.broadcast('SERVICE_DEREGISTERED', socket.serviceInfo)
      })
    })

    cb(null, service._id)
  })
}

const _registerSpectator = function (socket, data, cb) {
  const _this = this

  socket.type = data.type = _this.CLIENT_TYPES.SPECTATOR
  _this.emit('spectator-registered')

  socket.on('disconnect', () => {
    _this.emit('spectator-deregistered')
  })

  cb()
}

module.exports = {
  hpcs: {
    register: function (req, res) {
      const _this = this

      req.__socket.setTimeout(0)

      switch (req.data.type) {
        case _this.CLIENT_TYPES.DISCOVERY_SERVICE:
          return _registerDiscoveryService.call(_this, req.__socket, req.data, (err, id) => {
            if (err) {
              return res.sendError(err)
            }

            res.send({ serviceId: id, discoveryBackendUri: core.config.discoveryBackendUri })
          })
        case _this.CLIENT_TYPES.SERVICE:
          return _registerService.call(_this, req.__socket, req.data, (err, id) => {
            if (err) {
              return res.sendError(err)
            }

            res.send({ serviceId: id })
          })
        case _this.CLIENT_TYPES.SPECTATOR:
          return _registerSpectator.call(_this, req.__socket, req.data, err => {
            if (err) {
              return res.sendError(err)
            }

            res.send()
          })
        default:
          return res.sendError('Type specified invalid', () => {
            req.__socket.destroy()
          })
      }
    },
    getServices: (req, res) => {
      dataHandlers.services.get(req.data.type, req.data.version, (err, services) => {
        if (err) {
          return res.sendError(err)
        }

        res.send(services)
      })
    },
    getDbServer: (req, res) => {
      db.getDb().dbs.findOne({ _id: req.data.name }, (err, data) => {
        if (err) {
          return res.sendError(err)
        }

        res.send(data)
      })
    },
    getEnvVar: (req, res) => {
      dataHandlers.envVars.get(req.data.key, (err, value) => {
        if (err) {
          return res.sendError(err)
        }

        res.send(value)
      })
    },
    ping: (req, res) => {
      res.send('pong')
    }
  },
  rpcs: [],
  registerEventListeners: function (tcpServer) {
    const _this = this

    _this.on('discovery-master-found', info => {
      tcpServer.broadcast('DISCOVERY_MASTER_FOUND', info.tcpServer)
    })

    _this.on('instructed-to-become-disco-slave', info => {
      tcpServer.broadcast('DISCOVERY_MASTER_FOUND', info)
    })
  }
}
