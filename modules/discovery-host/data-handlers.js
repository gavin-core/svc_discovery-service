const core = require('node-core')
const mongojs = require('mongojs')
const db = require('../../middleware/db')
const enums = require('./enums')

const Version = core.Version
const _ = core._

let _autoStartServiceLock = false

module.exports = {
  autoStartServiceHosts: {
    get: (name, version, cb) => {
      db.getDb().auto_start_service_hosts.findOne({ _id: name + '/' + version }, cb)
    },
    add: function (discoData, autoStartServiceConfig, cb) {
      if (_autoStartServiceLock) {
        return setTimeout(function () {
          module.exports.autoStartServiceHosts.add(discoData, autoStartServiceConfig, cb)
        }, 50)
      }

      _autoStartServiceLock = true
      autoStartServiceConfig = autoStartServiceConfig || []

      db.getDb().auto_start_service_hosts.find((err, data) => {
        if (err) {
          console.log('Error received but not catered for')
          console.error(err)
        }

        for (let i = 0; i < autoStartServiceConfig.length; i++) {
          if (!autoStartServiceConfig[i].enabled) {
            continue
          }

          let entry = null

          for (let j = 0; j < data.length; j++) {
            if (data[j]._id === (`${autoStartServiceConfig[i].name}/${autoStartServiceConfig[i].version}`)) {
              entry = data[j]
              break
            }
          }

          if (!entry) {
            entry = {
              _id: `${autoStartServiceConfig[i].name}/${autoStartServiceConfig[i].version}`,
              hosts: []
            }
          }

          entry.hosts.push({
            _id: discoData._id,
            host: discoData.host,
            max: autoStartServiceConfig[i].max
          })

          db.getDb().auto_start_service_hosts.save(entry, () => {})
        }
        _autoStartServiceLock = false
        ;(cb || (() => {}))()
      })
    },
    remove: (serviceId, cb) => {
      db.getDb().auto_start_service_hosts.find((err, data) => {
        if (err) {
          throw err
        }

        for (let i = 0; i < data.length; i++) {
          let changes = false

          for (let j = data[i].hosts.length - 1; j >= 0; j--) {
            const host = data[i].hosts[j]
            if (serviceId.toString() === host._id.toString()) {
              data[i].hosts.splice(j, 1)
              changes = true
            }
          }

          if (changes) {
            db.getDb().auto_start_service_hosts.save(data[i])
          }
        }

        ;(cb || (() => {}))()
      })
    },
    clear: cb => {
      db.getDb().auto_start_service_hosts.remove({}, cb)
    }
  },
  autoStartServices: {},
  envVars: {
    get: (key, cb) => {
      db.getDb().vars.findOne({ _id: key }, (err, entry) => {
        if (err || !entry) {
          return cb(err)
        }
        return cb(null, entry.value)
      })
    }
  },
  services: {
    get: (name, version, cb) => {
      if (version === '*') {
        version = ''
      }

      version = new core.Version(version, '*')

      const query = {
        'name': name,
        'version': {
          $regex: '^' + version.toString() + '$'
        }
      }

      db.getDb().services.find(query, (err, services) => {
        if (err) {
          return cb(new Error(`Error obtaining service: ${err}`))
        }

        if (!services || !services.length) {
          return cb(null, [])
        }

        const versions = _.pluck(services, 'version')
        const maxVersion = new Version(version.maxMatch(versions), '0')

        const servs = []
        for (let i = 0; i < services.length; i++) {
          if (maxVersion.matches(services[i].version)) {
            servs.push(services[i])
          }
        }

        cb(null, servs)
      })
    },
    saveDiscovery: (data, cb) => {
      cb = cb || (() => {})

      const discoModel = {
        _id: (data._id || new mongojs.ObjectID()).toObjectID(),
        name: 'DiscoveryService',
        type: data.type,
        host: data.host,
        version: new Version(data.version, '0').toString(),
        dateStarted: data.dateStarted || +new Date(),
        dateRegistered: +new Date()
      }

      db.getDb().services.save(discoModel, err => {
        if (err) {
          return cb(err)
        }

        cb(null, discoModel)
      })
    },
    removeDiscovery: (criteria, cb) => {
      criteria = _.extend(criteria, { type: enums.CLIENT_TYPES.DISCOVERY_SERVICE })
      db.getDb().services.remove(criteria, cb)
    },
    saveService: (data, cb) => {
      cb = cb || (() => {})

      data._id = (data.serviceId || new mongojs.ObjectID()).toObjectID()
      data.dateStarted = data.dateStarted || +new Date()
      data.dateRegistered = +new Date()

      db.getDb().services.save(data, (err, service) => {
        if (err) {
          return cb(err)
        }

        cb(null, service)
      })
    },
    removeServices: (criteria, cb) => {
      criteria = _.extend(criteria, { type: enums.CLIENT_TYPES.SERVICE })
      db.getDb().services.remove(criteria, cb)
    }
  }
}
