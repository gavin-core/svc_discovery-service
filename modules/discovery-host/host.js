const events = require('events')
const core = require('node-core')
const networking = require('node-network')
const mongojs = require('mongojs')
const enums = require('./enums')
const tcpServerHandlers = require('./tcp-server-handlers')
const tcpClientHandlers = require('./tcp-client-handlers')
const udpRPCs = require('./udp-rpcs')
const db = require('../../middleware/db')
const AutoStartServiceManager = require('./auto-start-service-manager')
const autoStartServiceManagerHandlers = require('./auto-start-service-manager-handlers')
const servicePackage = require('../service-package')

const addressUtils = networking.addressUtils
const config = core.config
const logger = core.logger
const UdpSocket = networking.UdpSocket
const _ = core._

const Host = function (options, tcpServerHPCs) {
  const _this = this
  const _retriesForDiscoMaster = 3
  const _discoMasterFindIntervalMS = 1000

  let _timeout
  let _discoMasterFindInterval
  let _autoStartServiceManager
  let _tcpServerHPCs = tcpServerHandlers.hpcs

  _this.isMaster = false
  _this.preferredIPMask = options.preferredIPMask
  _this.udp = options.udp
  _this.priority = options.priority || 15
  _this.dateStarted = +new Date()

  if (!config.hasOwnProperty('broadcastableNetwork')) {
    throw new Error('Expected config to have a property \'broadcastableNetwork\'')
  }

  _this.broadcastableNetwork = !((config.broadcastableNetwork === false || config.broadcastableNetwork === 0 || config.broadcastableNetwork === 'false' || config.broadcastableNetwork === '0'))

  _this.CLIENT_TYPES = enums.CLIENT_TYPES

  _this.serviceInfo = {
    _id: new mongojs.ObjectID(),
    name: servicePackage.name,
    version: servicePackage.version,
    type: _this.CLIENT_TYPES.DISCOVERY_SERVICE,
    host: null,
    autoStartServiceConfig: config.autoStartServices,
    dateStarted: _this.dateStarted
  }

  for (let i = 0; i < (tcpServerHPCs || []).length; i++) {
    _tcpServerHPCs = core._.extend(tcpServerHandlers.hpcs, tcpServerHPCs[i])
  }

  _this.tcpServer = new networking.SocketServer({ host: process.network.myAddress }, _tcpServerHPCs, tcpServerHandlers.rpcs, _this)

  _this.tcpServer.on('listening', () => {
    logger.debug('TCP server listening on ' + _this.tcpServer.address().address + ':' + _this.tcpServer.address().port)
    _this.serviceInfo.host = _this.tcpServer.address()
  })

  _this.tcpServer.on('connection', socket => {
    logger.debug('client connected to TCP')
    socket.setTimeout(6000, () => {
      socket.sendJson('SAFE_KILL', { msg: 'Timeout to register expired, destroying socket' }, () => {
        socket.setTimeout(2000, socket.destroy)
      })
    })
    socket.type = _this.CLIENT_TYPES.SPECTATOR
    socket.on('close', () => {
      logger.debug('client disconnected')
    })
  })

  tcpServerHandlers.registerEventListeners.call(_this, _this.tcpServer)
  _this.tcpServer.listen()

  /// //////////////////// UDP BROADCAST SOCKET ///////////////////////
  const broadcastSocketOptions = {
    address: process.network.myAddress,
    broadcastAddress: process.network.broadcastAddress,
    port: _this.udp.port
  }

  _this.broadcastSocket = new UdpSocket(broadcastSocketOptions, udpRPCs, _this)

  _this.broadcastSocket.on('listening', address => {
    _this.searchForMaster()
  })
  _this.broadcastSocket.on('error', err => {
    logger.error(err)
    process.kill(process.pid)
  })
  _this.broadcastSocket.on('unknownMessage', () => {
    console.log('unknownMessage: ' + JSON.stringify(arguments))
  })
  _this.broadcastSocket.start()
  /// //////////////////// END UDP BROADCAST SOCKET ///////////////////////

  _this.enterMasterMode = () => {
    db.setDbUri(core.config.discoveryBackendUri)

    if (_this.tcpClient) {
      _this.tcpClient.destroy()
    }

    _this.tcpClient = null

    _this.isMaster = true
    _this.emit('master-mode-enabled')

    if (!_this.serviceInfo._id) {
      _this.serviceInfo._id = new mongojs.ObjectID()
    }

    _autoStartServiceManager = new AutoStartServiceManager(_this.serviceInfo, config.autoStartServices)
    autoStartServiceManagerHandlers.registerEventListeners.call(_autoStartServiceManager, _this.serviceInfo._id, _this.tcpServer)
  }

  _this.enterSlaveMode = tcpInfo => {
    if (_this.isMaster) {
      _this.emit('slave-mode-enabled')
    }

    _this.isMaster = false

    if (_autoStartServiceManager) {
      _autoStartServiceManager.destroy()
      _autoStartServiceManager = null
    }

    const slaveDiscos = core._.where(_this.tcpServer.sockets, {
      type: _this.CLIENT_TYPES.DISCOVERY_SERVICE
    })

    for (let i = 0; i < slaveDiscos.length; i++) {
      slaveDiscos[i].end()
    }

    if (!tcpInfo) {
      return _this.searchForMaster()
    }

    if (_this.tcpClient) {
      _this.tcpClient.destroy()
    }

    tcpInfo.host = tcpInfo.address
    _this.tcpClient = new networking.SocketClient(tcpInfo, tcpClientHandlers.hpcs, tcpClientHandlers.rpcs)

    _this.tcpClient.on('connect', () => {
      _this.tcpClient.rpcs.register(_this.serviceInfo, (err, data) => {
        if (err) {
          throw new Error('Invalid service type specified for discovery registration')
        }

        _this.serviceInfo._id = data.serviceId
        db.setDbUri(data.discoveryBackendUri)
      })
      _this.emit('connected-to-disco-master')
    })
    _this.tcpClient.on('error', err => {
      logger.error(err)
      _this.searchForMaster()
    })
    _this.tcpClient.on('disconnect', () => {
      _this.searchForMaster()
    })

    tcpClientHandlers.registerEventListeners.call(_this, _this.tcpClient)

    return _this
  }

  _this.on('discovery-master-found', () => {
    clearInterval(_discoMasterFindInterval)
  })

  _this.searchForMaster = () => {
    if (_this.isMaster) {
      return
    }

    _this.emit('searching-for-disco-master')

    const sendSearch = () => {
      if (_this.broadcastableNetwork) {
        return _this.broadcastSocket.send('SEARCHING_FOR_DISCO_MASTER')
      }

      if (!addresses || !addresses.length) {
        addresses = addressUtils.getAddressesInRange()
      }

      for (let i = 0; i < addresses.length; i++) {
        _this.broadcastSocket.send('SEARCHING_FOR_DISCO_MASTER', null, { address: addresses[i] })
      }
    }

    let retries = 0
    let addresses

    _discoMasterFindInterval = setInterval(() => {
      retries++
      if (retries < _retriesForDiscoMaster) {
        return sendSearch()
      }

      clearInterval(_discoMasterFindInterval)
      _this.enterMasterMode()
    }, _discoMasterFindIntervalMS)

    return sendSearch()
  }

  _this.stopService = (serviceId, cb) => {
    if (!_this.isMaster) {
      return cb(new Error('Not currently master, can\'t handle your request'))
    }

    const socket =_this.tcpServer.sockets.find(socket => 
      socket.type === _this.CLIENT_TYPES.SERVICE && 
      socket.serviceId.toString() === serviceId.toString()
    )

    if (!socket) {
      return cb(new Error(`No service with id:${serviceId} found`))
    }

    socket.sendJson('SAFE_KILL')
    cb()
  }

  _this.terminate = cb => {
    if (!_this.isMaster) {
      return cb()
    }

    if (!_.findWhere(_this.tcpServer.sockets, { type: _this.CLIENT_TYPES.DISCOVERY_SERVICE })) {
      /*
      return _this.tcpServer.broadcast('SAFE_KILL', null, null, () => {
        db.getDb().services.remove({}, cb)
      })
      */
      return db.getDb().services.remove({}, cb)
    }

    let newMaster
    _this.selectNewMaster = (priority, reqCb) => {
      clearTimeout(_timeout)
      if (!newMaster) {
        newMaster = { priority: priority, cb: reqCb || Object.__noop }
      }

      if (+newMaster.priority < priority) {
        newMaster.cb()
        newMaster = { priority: priority, cb: reqCb || Object.__noop }
      }

      _timeout = setTimeout(() => {
        _this.selectNewMaster = null

        db.getDb().services.remove({ type: _this.CLIENT_TYPES.DISCOVERY_SERVICE }, () => {
          newMaster.cb(true)
          cb()
        })
      }, _discoMasterFindIntervalMS)
    }

    _timeout = setTimeout(() => {
      db.getDb().services.remove(cb)
    }, 10000)

    _this.tcpServer.broadcast('WHO_WANTS_TO_BE_MASTER', null, { type: _this.CLIENT_TYPES.DISCOVERY_SERVICE })
  }

  return _this
}

Host.prototype = events.EventEmitter.prototype
module.exports = Host
