const events = require('events')
const core = require('node-core')
const Host = require('./host')

const logger = core.logger

const DiscoveryHost = function (options, tcpServerRPCs) {
  const _this = this
  const host = new Host(options, tcpServerRPCs)

  _this.isMaster = host.isMaster
  _this.CLIENT_TYPES = host.CLIENT_TYPES
  _this.stopService = host.stopService
  _this.terminate = host.terminate
  _this.discovery = {}
  _this.serviceInfo = host.serviceInfo

  host.on('connected-to-disco-master', () => {
    logger.debug('Connected To Disco Master')
    _this.discovery = host.tcpClient.rpcs
    _this.emit('connected-to-disco-master')
  })
  host.on('master-mode-enabled', () => {
    logger.debug('Master Mode Enabled')
    _this.discovery = {}
    _this.emit('master-mode-enabled')
  })
  host.on('slave-mode-enabled', () => {
    if (!_this.isMaster) {
      return
    }

    logger.debug('Slave Mode Enabled')
    _this.emit('slave-mode-enabled')
  })
  host.on('searching-for-disco-master', () => {
    logger.debug('Searching For Disco Master')
    _this.emit('searching-for-disco-master')
  })
  host.on('discovery-master-found', () => {
    logger.debug('Disco Master Found')
    _this.emit('discovery-master-found')
  })
  host.on('precedence-as-disco-master', () => {
    logger.debug('Taking Over As Disco Master')
    _this.emit('precedence-as-disco-master')
  })
  host.on('instructed-to-become-disco-master', () => {
    logger.debug('Instructed To Become Disco Master')
    _this.emit('instructed-to-become-disco-master')
  })
  host.on('instructed-to-become-disco-slave', () => {
    logger.debug('Instructed To Become Disco Slave')
    _this.emit('instructed-to-become-disco-slave')
  })

  host.on('discovery-registered', () => {
    logger.debug('discovery-registered')
  })
  host.on('service-registered', () => {
    logger.debug('service-registered')
  })
  host.on('service-registered-with-slave-disco', () => {
    logger.debug('service-registered-with-slave-disco')
  })

  host.on('discovery-deregistered', () => {
    logger.debug('discovery-deregistered')
  })

  host.on('service-deregistered', () => {
    logger.debug('service-deregistered')
  })

  host.on('service-deregistered-with-slave-disco', () => {
    logger.debug('service-deregistered-with-slave-disco')
  })

  return _this
}

DiscoveryHost.prototype = events.EventEmitter.prototype
module.exports = (options, tcpServerRPCs) => {
  module.exports = new DiscoveryHost(options, tcpServerRPCs)
  return module.exports
}
