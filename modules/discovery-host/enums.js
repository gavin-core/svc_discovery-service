module.exports = {
  CLIENT_TYPES: {
    DISCOVERY_SERVICE: 1,
    SERVICE: 2,
    SPECTATOR: 4
  }
}
