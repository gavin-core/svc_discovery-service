const core = require('node-core')
const childProcess = require('child_process')
const path = require('path')

const config = core.config
const Version = core.Version
const _ = core._

module.exports = {
  startService: (name, version, count) => {
    const autoStartServiceConfigs = _.filter(config.autoStartServices, entry => name === entry.name)
    const versions = _.pluck(autoStartServiceConfigs, 'version')
    const maxVersion = new Version(version).maxMatch(versions)
    const autoStartServiceConfig = _.findWhere(autoStartServiceConfigs, entry => new Version(entry.version).isSamePart(maxVersion, 'build'))

    for (let i = 0; i < count; i++) {
      childProcess.spawn(
        autoStartServiceConfig.app,
        [path.join(autoStartServiceConfig.path, autoStartServiceConfig.file)].concat(autoStartServiceConfig.args || []),
        { detached: true, stdio: [ 'ignore', 'ignore', 'ignore' ] }
      ).unref()
    }
  }
}
