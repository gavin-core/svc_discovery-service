const core = require('node-core')
const dataHandlers = require('./data-handlers')
const enums = require('./enums')
const parentProcess = require('./parent-process')

const logger = core.logger
const _ = core._

module.exports = {
  registerEventListeners: function (myServiceId, tcpServer) {
    // 'this' is the AutoStartServiceManager
    const _this = this
    const _myServiceId = myServiceId.toString()
    const _tcpServer = tcpServer

    _this.on('safe-kill-service', data => {
      if (data.host.address === tcpServer.address().address) {
        const socket = _.find(tcpServer.sockets, socket => socket.type === enums.CLIENT_TYPES.SERVICE && socket.serviceId.toString() === data._id.toString())
        return socket.sendJson('SAFE_KILL')
      }

      const socket = _.find(_tcpServer.sockets, socket => socket.type === enums.CLIENT_TYPES.DISCOVERY_SERVICE && socket.serviceId.toString() === data.host.serviceId.toString())
      socket.sendJson('SAFE_KILL_SERVICE', { serviceId: data._id.toString() })
    })

    _this.on('start-service', data => {
      dataHandlers.autoStartServiceHosts.get(data.name, data.version, (err, hosts) => {
        if (err) {
          return logger.error(err)
        }

        if (!hosts) {
          return logger.info(`Wanted to start a ${data.name}/${data.version} but no disco's available that can start it`)
        }

        hosts = hosts.hosts

        if (!hosts.length) {
          return logger.info(`Wanted to start a ${data.name}/${data.version} but no disco's available that can start it`)
        }

        const hostInfoByServiceId = {}

        for (let i = 0; i < hosts.length; i++) {
          hostInfoByServiceId[hosts[i]._id.toString()] = {
            hosted: 0,
            max: hosts[i].max
          }
        }

        dataHandlers.services.get(data.name, data.version, (err, services) => {
          if (err) {
            return logger.error(err)
          }

          for (let i = 0; i < services.length; i++) {
            hostInfoByServiceId[services[i].host.serviceId.toString()].hosted++
          }

          for (let i = 0; i < data.count; i++) {
            let serviceId = null
            let serviceIds = _.keys(hostInfoByServiceId)

            for (let j = serviceIds.length - 1; j >= 0; j--) {
              if (hostInfoByServiceId[serviceIds[j]].hosted >= hostInfoByServiceId[serviceIds[j]].max) {
                delete hostInfoByServiceId[serviceIds[j]]
                serviceIds = _.keys(hostInfoByServiceId)
              }
            }

            for (let j = 0; j < serviceIds.length; j++) {
              if (hostInfoByServiceId[serviceIds[j]].hosted < hostInfoByServiceId[serviceIds[j]].max) {
                if (serviceId === null || hostInfoByServiceId[serviceIds[j]].hosted < hostInfoByServiceId[serviceId].hosted) {
                  serviceId = serviceIds[j]
                }
              }
            }

            if (!serviceId) {
              logger.info(`All discovery services enabled to host a ${data.name} are running the max amount`)
              return
            }

            hostInfoByServiceId[serviceId].hosted++

            if (serviceId === _myServiceId) {
              parentProcess.startService(data.name, data.version, 1, data.args || [])
              continue
            }

            var socket = _.find(_tcpServer.sockets, socket => socket.serviceId.toString() === serviceId.toString())

            socket.sendJson('START_SERVICE', _.extend({}, data, { count: 1 }))
          }
        })
      })
    })
  }
}
