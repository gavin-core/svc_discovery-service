const core = require('node-core')
const enums = require('./enums')
const parentProcess = require('./parent-process')

const logger = core.logger
const Version = core.Version
const _ = core._

module.exports = {
  hpcs: {},
  rpcs: ['REQ_TAKEOVER_AS_MASTER', 'SERVICE_REGISTERED', 'SERVICE_DEREGISTERED', 'REGISTER', 'GET_SERVICES'],
  registerEventListeners: function (socket) {
    const _this = this
    const _socket = socket

    _socket.on('WHO_WANTS_TO_BE_MASTER', () => {
      _socket.rpcs.reqTakeoverAsMaster(err => {
        if (err) {
          return
        }

        _this.emit('instructed-to-become-disco-master')
        _this.enterMasterMode()
      })
    })

    _this.on('service-registered', eventInfo => {
      _socket.rpcs.serviceRegistered(eventInfo)
    })
    _this.on('service-deregistered', eventInfo => {
      _socket.rpcs.serviceDeregistered(eventInfo)
    })

    _socket.on('DISCOVERY_SERVICE_REGISTERED', eventInfo => {
      _this.tcpServer.broadcast.callApply(_this.tcpServer.broadcast, 'DISCOVERY_SERVICE_REGISTERED', eventInfo.data)
    })

    _socket.on('DISCOVERY_SERVICE_DEREGISTERED', eventInfo => {
      _this.tcpServer.broadcast.callApply(_this.tcpServer.broadcast, 'DISCOVERY_SERVICE_DEREGISTERED', eventInfo.data)
    })

    _socket.on('SERVICE_REGISTERED', eventInfo => {
      _this.tcpServer.broadcast.callApply(_this.tcpServer.broadcast, 'SERVICE_REGISTERED', eventInfo.data)
    })

    _socket.on('SERVICE_DEREGISTERED', eventInfo => {
      _this.tcpServer.broadcast.callApply(_this.tcpServer.broadcast, 'SERVICE_DEREGISTERED', eventInfo.data)
    })

    _socket.on('SAFE_KILL', () => {
      logger.info('Discovery master has requested my termination')
      process.kill(process.pid)
    })

    _socket.on('SAFE_KILL_SERVICE', eventInfo => {
      if (eventInfo.data.serviceId) {
        const socket = _.find(_this.tcpServer.sockets, socket => socket.type === enums.CLIENT_TYPES.SERVICE && socket.serviceId.toString() === eventInfo.data.serviceId.toString())

        if (!socket) {
          return
        }

        return socket.sendJson('SAFE_KILL')
      }

      const services = _.where(_this.tcpServer.sockets, socket =>
        socket.type === enums.CLIENT_TYPES.SERVICE &&
        socket.serviceInfo.name === eventInfo.data.name &&
        new Version(eventInfo.data.version).isSamePart(socket.serviceInfo.version, 'build')
      )

      for (let i = services.length - 1; i >= 0 && !!eventInfo.data.count; i--, eventInfo.data.count--) {
        services[i].sendJson('SAFE_KILL')
      }
    })

    _socket.on('START_SERVICE', eventInfo => {
      parentProcess.startService(eventInfo.data.name, eventInfo.data.version, eventInfo.data.count)
    })
  }
}
