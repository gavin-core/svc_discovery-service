const core = require('node-core')

module.exports = {
  'SEARCHING_FOR_DISCO': function (req, res) {
    const _this = this

    if (!_this.isMaster) {
      return
    }

    const discoOnSameIP = core._.find(this.tcpServer.sockets, disco => disco.type === _this.CLIENT_TYPES.DISCOVERY_SERVICE && disco.host.address === req.__socket.address)

    const respData = {
      tcpServer: discoOnSameIP && discoOnSameIP.host ? discoOnSameIP.host : _this.tcpServer.address(),
      network: process.network,
      priority: _this.priority
    }

    delete respData.network.myAddress

    res.send('SEARCHING_FOR_DISCO_RESP', respData)
  },
  'SEARCHING_FOR_DISCO_MASTER': function (req, res) {
    const _this = this

    if (!_this.isMaster) {
      return
    }

    const respData = {
      tcpServer: _this.tcpServer.address(),
      network: process.network,
      priority: _this.priority
    }

    delete respData.network.myAddress

    res.send('SEARCHING_FOR_DISCO_MASTER_RESP', respData)
  },
  'SEARCHING_FOR_DISCO_MASTER_RESP': function (req, res) {
    const _this = this

    _this.emit('discovery-master-found', req.data)

    if (+_this.priority >= +req.data.priority) {
      return _this.enterSlaveMode(req.data.tcpServer)
    }

    _this.emit('precedence-as-disco-master')
    _this.enterMasterMode()
    res.send('BECOME_SLAVE', _this.tcpServer.address())
  },
  'BECOME_SLAVE': function (req, res) {
    const _this = this
    _this.emit('instructed-to-become-disco-slave', req.data)
    _this.enterSlaveMode(req.data)
  }
}
