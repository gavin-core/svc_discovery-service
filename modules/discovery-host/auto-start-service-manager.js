const events = require('events')

const AutoStartServiceManager = function (serviceInfo, localAutoStartServiceConfigs) {
  const core = require('node-core')
  const db = require('../../middleware/db')
  const enums = require('./enums')
  const dataHandlers = require('./data-handlers')

  const logger = core.logger
  const Version = core.Version
  const _ = core._

  const _this = this
  const _localAutoStartServiceConfigs = localAutoStartServiceConfigs
  const _serviceInfo = serviceInfo || {}
  let _inspectInterval

  const _initialize = () => {
    dataHandlers.autoStartServiceHosts.clear(err => {
      if (err) {
        logger.error(err)
        return _initialize()
      }

      dataHandlers.autoStartServiceHosts.add(_serviceInfo, _localAutoStartServiceConfigs)
    })
  }

  const _inspectEnvironment = () => {
    [
      function getAutoStartConfigsFromDB (next, data) {
        db.getDb().auto_start_services.find({ enabled: true }, (err, autoStartServices) => {
          if (err) {
            return next(err)
          }

          data.autoStartServices = autoStartServices
          next()
        })
      },
      function getRegisteredServicesFromDB (next, data) {
        db.getDb().services.find({ type: enums.CLIENT_TYPES.SERVICE }, function (err, services) {
          if (err) {
            return next(err)
          }

          data.services = services
          next()
        })
      },
      function compareRequiredWithAvailable (next, data) {
        for (let i = 0; i < data.autoStartServices.length; i++) {
          const relatedServices = _.filter(data.services, service => data.autoStartServices[i].name === service.name && new Version(data.autoStartServices[i].version).matches(service.version))
            .sort((a, b) => b.version - a.version || b.dateStarted - a.dateStarted)

          for (let j = relatedServices.length - 1; j >= data.autoStartServices[i].max; j--) {
            _this.emit('safe-kill-service', relatedServices[j])
          }

          if (relatedServices.length < data.autoStartServices[i].min) {
            _this.emit('start-service', { name: data.autoStartServices[i].name, version: data.autoStartServices[i].version, count: data.autoStartServices[i].min - relatedServices.length })
          }
        }

        next()
      }
    ].callInSequence(err => {
      if (err) {
        logger.error(err)
      }
    })
  }

  _this.destroy = () => {
    clearInterval(_inspectInterval)
  }

  _inspectInterval = setInterval(_inspectEnvironment, 20000)
  _initialize()

  return _this
}

AutoStartServiceManager.prototype = events.EventEmitter.prototype
module.exports = AutoStartServiceManager
