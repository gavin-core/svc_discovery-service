const core = require('node-core')
const path = require('path')
const web = require('node-web')
const mongojs = require('mongojs')
const db = require('../../../middleware/db')

const logger = core.logger

module.exports = (DiscoveryHost, cookieConfig, jwtConfig, crypto) => {
  if (!cookieConfig) {
    throw new Error('No cookie config provided')
  }

  if (!cookieConfig.name) {
    throw new Error('No cookie name config provided')
  }

  if (!cookieConfig.domain) {
    throw new Error('No cookie domain config provided')
  }

  if (!cookieConfig.secret) {
    throw new Error('No cookie secret config provided')
  }

  if (!jwtConfig) {
    throw new Error('No jwt config provided')
  }

  if (!jwtConfig.secret) {
    throw new Error('No jwt secret config provided')
  }

  const conf = {
    host: '0.0.0.0',
    port: 5000
  }

  const configWebCb = app => {
    web.enableStaticFiles(app, [
      ['/code', path.join(__dirname, '../app/code'), true],
      ['/media', path.join(__dirname, '../app/media'), true],
      ['/views', path.join(__dirname, '../app/views'), true],
      ['/bower_components', path.join(__dirname, '../bower_components'), true]
    ])

    web.enableCookieParser(app, cookieConfig.secret)
    app.use((req, res, next) => {
      if (!req.signedCookies || !req.signedCookies[cookieConfig.name]) {
        return next()
      }

      db.getDb().users.findOne({ _id: mongojs.ObjectId(req.signedCookies[cookieConfig.name]) }, (err, user) => {
        if (err || !user) {
          return next()
        }

        req.user = user

        return next()
      })
    })

    web.enableSendExpressResponder(app, true)
    web.enableUserAgent(app)
    web.enableRequestTracking(app)
    web.viewEngines.ejs(app, path.join(__dirname, 'views'), true)

    app.get('/login', (req, res) => {
      if (req.user) {
        return res.sendexpress.temporaryRedirect('/')
      }

      return res.render('login.ejs')
    })

    app.use((req, res, next) => {
      if (!req.user) {
        return res.sendexpress.temporaryRedirect('/login')
      }

      next()
    })

    app.get('/', (req, res) => {
      res.render('index.ejs')
    })

    app.use((req, res, next) => {
      res.sendexpress.temporaryRedirect('/')
    })
  }

  const onListeningCb = (err, serverInfo) => {
    if (err) {
      console.log('Error received but not catered for')
      console.error(err)
    }

    logger.debug(`Web server listening on ${serverInfo.host}:${serverInfo.port}`)
  }

  web.createServer(conf, configWebCb, onListeningCb)
}
