module.exports = function (DiscoveryHost) {
  const server = require('./server/index.js')
  return server.apply(server, arguments)
}
