window.discoApp.service('AuthService', ['$window', '$q', '$timeout', '$cookies', '$location', 'apiResource', 'AuthorizeLoader', function ($window, $q, $timeout, $cookies, $location, apiResource, AuthorizeLoader) {
  const _this = this

  _this.parseJwt = token => {
    const deferred = $q.defer()

    $timeout(() => {
      const base64Url = token.split('.')[1]
      const base64 = base64Url.replace('-', '+').replace('_', '/')
      const parsed = JSON.parse($window.atob(base64))
      deferred.resolve(parsed)
    })

    return deferred.promise
  }

  _this.saveToken = token => {
    const deferred = $q.defer()

    $timeout(() => {
      $window.localStorage['jwtToken'] = token
      deferred.resolve()
    })

    return deferred.promise
  }

  _this.logout = () => apiResource('/logout')
    .get()
    .$promise
    .catch(err => {
      if (err.status === 502) {
        // Service offline
        return
      }

      throw err
    })
    .then(() => {
      $window.localStorage.removeItem('jwtToken')
      $window.location.replace('/')
    })

  _this.getToken = authorizeIfNecessary => {
    const deferred = $q.defer()

    $timeout(() => {
      const token = $window.localStorage['jwtToken']

      if (!authorizeIfNecessary) {
        return deferred.resolve(token)
      }

      _this.isAuthed()
        .then(authed => {
          if (authed) {
            return deferred.resolve(token)
          }

          AuthorizeLoader()
            .$promise
            .then(res => {
              _this.saveToken(res.token)
              return res
            }, () => _this.logout())
            .then(res => deferred.resolve(res.token))
            .catch(err => deferred.reject(err))
        })
    })

    return deferred.promise
  }

  _this.isAuthed = () => _this.getToken(false)
    .then(token => {
      if (!token) {
        return false
      }

      return _this.parseJwt(token)
    })
    .then(params => params && Math.round(new Date().getTime() / 1000) <= params.exp)
}])
