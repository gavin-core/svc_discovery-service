window.discoApp.factory('DbsRoute', ['apiResource', function (apiResource) {
  return apiResource('/dbs')
}])
.factory('DbsLoader', ['$q', '$timeout', 'DbsRoute', 'DbInstance', function ($q, $timeout, DbsRoute, DbInstance) {
  return function (params) {
    return DbInstance.fromArray(DbsRoute.query(params))
  }
}])
