window.discoApp.factory('ServicesRoute', ['apiResource', function (apiResource) {
  return apiResource('/services/:id', { id: '@_id' })
}])
.factory('ServicesLoader', ['$q', '$timeout', 'ServicesRoute', 'ServiceInstance', function ($q, $timeout, ServicesRoute, ServiceInstance) {
  return function (params) {
    return ServiceInstance.fromArray(ServicesRoute.query(params))
  }
}])
