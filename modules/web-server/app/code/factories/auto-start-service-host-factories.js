window.discoApp.factory('AutoStartServiceHostsRoute', ['apiResource', function (apiResource) {
  return apiResource('/autostartservicehosts')
}])
.factory('AutoStartServiceHostsLoader', ['$q', '$timeout', 'AutoStartServiceHostsRoute', function ($q, $timeout, AutoStartServiceHostsRoute) {
  return function (params) {
    return AutoStartServiceHostsRoute.query(params)
  }
}])
