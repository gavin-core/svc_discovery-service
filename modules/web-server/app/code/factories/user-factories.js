window.discoApp.factory('UsersRoute', ['apiResource', apiResource => {
  return apiResource('/users/:id')
}])
.factory('UsersLoader', ['UsersRoute', 'UserInstance', (UsersRoute, UserInstance) => {
  return params => UserInstance.fromArray(UsersRoute.query(params))
}])
.factory('UserCreator', ['UsersRoute', UsersRoute => {
  return user => UsersRoute.save(user)
}])
