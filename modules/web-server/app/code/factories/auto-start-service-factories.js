window.discoApp.factory('AutoStartServicesRoute', ['apiResource', function (apiResource) {
  return apiResource('/autostartservices/:id', { 
    id: '@_id' 
  }, {
    'update': { method:'PUT' }
  })
}])
.factory('AutoStartServicesLoader', ['$q', '$timeout', 'AutoStartServicesRoute', 'AutoStartServiceInstance', function ($q, $timeout, AutoStartServicesRoute, AutoStartServiceInstance) {
  return function (params) {
    return AutoStartServiceInstance.fromArray(AutoStartServicesRoute.query(params))
  }
}])
