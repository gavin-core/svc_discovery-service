window.discoApp.factory('apiResource', ['$resource', function ($resource) {
  return function (path, params) {
    return $resource('/api' + path, params)
  }
}])
