window.discoApp.factory('AuthorizeRoute', ['apiResource', function (apiResource) {
  return apiResource('/authorize')
}])
.factory('AuthorizeLoader', ['$q', '$timeout', 'AuthorizeRoute', 'AuthorizeInstance', function ($q, $timeout, AuthorizeRoute, AuthorizeInstance) {
  return function (params) {
    return new AuthorizeInstance(AuthorizeRoute.get(params))
  }
}])
