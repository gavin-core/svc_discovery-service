window.discoApp.service('DbInstance', [function () {
  const DbInstance = function (db) {
    const _this = this

    window.angular.extend(_this, db)

    return _this
  }

  DbInstance.fromArray = function (arr) {
    for (let i = 0; i < arr.length; i++) {
      arr[i] = new DbInstance(arr[i])
    }

    return arr
  }

  return DbInstance
}])
