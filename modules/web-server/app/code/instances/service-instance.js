window.discoApp.service('ServiceInstance', ['InterfaceInstance', function (InterfaceInstance) {
  const ServiceInstance = function (db) {
    const _this = this

    window.angular.extend(_this, db)
    _this.interfaces = InterfaceInstance.fromArray(_this.interfaces)

    for (var key in db.__proto__) {
      if (/^\$\w/.test(key)) {
        _this[key] = db.__proto__[key]
      }
    }

    return _this
  }

  ServiceInstance.fromArray = function (arr) {
    if (arr.$promise && !arr.$resolved) {
      return arr.$promise.then(arr => ServiceInstance.fromArray(arr))
    }

    for (var i = 0; i < arr.length; i++) {
      arr[i] = new ServiceInstance(arr[i])
    }

    return arr
  }

  return ServiceInstance
}])
