window.discoApp.service('AutoStartServiceInstance', [function () {
  const AutoStartServiceInstance = function (db) {
    const _this = this

    window.angular.extend(_this, db)

    for (var key in db.__proto__) {
      if (/^\$\w/.test(key)) {
        _this[key] = db.__proto__[key]
      }
    }

    return _this
  }

  AutoStartServiceInstance.fromArray = function (arr) {
    if (arr.$promise && !arr.$resolved) {
      return arr.$promise.then(arr => AutoStartServiceInstance.fromArray(arr))
    }

    for (var i = 0; i < arr.length; i++) {
      arr[i] = new AutoStartServiceInstance(arr[i])
    }

    return arr
  }

  return AutoStartServiceInstance
}])
