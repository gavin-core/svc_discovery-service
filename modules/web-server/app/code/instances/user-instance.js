window.discoApp.service('UserInstance', [function () {
  const UserInstance = function (db) {
    const _this = this

    window.angular.extend(_this, db)

    return _this
  }

  UserInstance.fromArray = function (arr) {
    for (let i = 0; i < arr.length; i++) {
      arr[i] = new UserInstance(arr[i])
    }

    return arr
  }

  return UserInstance
}])
