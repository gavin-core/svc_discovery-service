window.discoApp.service('InterfaceInstance', [function () {
  const InterfaceInstance = function (db) {
    const _this = this

    window.angular.extend(_this, db)

    _this.uri = `${_this.protocol}://${_this.host || _this.address}:${_this.port}`
    _this.easyUri = `(${_this.key}) ${_this.uri}`

    return _this
  }

  InterfaceInstance.fromArray = function (arr) {
    for (let i = 0; i < arr.length; i++) {
      arr[i] = new InterfaceInstance(arr[i])
    }

    return arr
  }

  return InterfaceInstance
}])
