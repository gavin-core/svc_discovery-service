window.discoApp.service('AuthorizeInstance', [function () {
  const AuthorizeInstance = function (auth) {
    const _this = this

    window.angular.extend(_this, auth)

    return _this
  }

  return AuthorizeInstance
}])
