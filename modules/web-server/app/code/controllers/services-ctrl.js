window.discoApp.controller('ServicesCtrl', ['$scope', '$state', '$mdToast', 'view', 'services', 'ServicesLoader', function ($scope, $state, $mdToast, view, services, ServicesLoader) {
  $scope.view = view || 'all'

  for (let i = 0; i < services.length; i++) {
    services[i].aliveDuration = +new Date() - services[i].dateStarted
  }

  $scope.services = services

  if (!services.length) {
    const simpleToast = $mdToast.simple()
      .textContent('No services currently running')
      .position('top right')
      .hideDelay(3000)
      .theme('warning')

    $mdToast.show(simpleToast)
  }

  $scope.killService = (service, $index) => {
    service.$delete()
      .then(res => {
        $scope.services.splice($index, 1)
      }, err => {
        // TODO 
      })
  }

  $scope.$watch('view', (newVal, oldVal) => {
    if (newVal === oldVal) {
      return
    }

    $state.go('app.services', { view: $scope.view }, { notify: false })
  })

  $scope.$root.topRightMenu.setItems([])
}])
