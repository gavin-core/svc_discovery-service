window.discoApp.controller('AutoStartServicesCtrl', ['$scope', 'autoStartServices', function ($scope, autoStartServices) {
  $scope.autoStartServices = autoStartServices

  $scope.onChange = (autoStartService, $index) => {
    autoStartService.$update()
      .then(() => {
        // No need to do anything
      }, err => {
        // TODO
      })
  }
  
  $scope.$root.topRightMenu.setItems([])
}])
