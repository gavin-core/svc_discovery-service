window.discoApp.controller('AutoStartServiceHostsCtrl', ['$scope', '$state', 'view', 'autoStartServiceHosts', function ($scope, $state, view, autoStartServiceHosts) {
  $scope.view = view || 'services'
  $scope.servicesByHost = []

  autoStartServiceHosts.$promise.then(function (results) {
    $scope.autoStartServiceHosts = results
    const servicesByHost = {}

    for (let i = 0; i < autoStartServiceHosts.length; i++) {
      for (let j = 0; j < autoStartServiceHosts[i].hosts.length; j++) {
        if (!servicesByHost[autoStartServiceHosts[i].hosts[j].host.address]) {
          servicesByHost[autoStartServiceHosts[i].hosts[j].host.address] = []
        }

        servicesByHost[autoStartServiceHosts[i].hosts[j].host.address].push({
          name: autoStartServiceHosts[i]._id,
          max: autoStartServiceHosts[i].hosts[j].max
        })
      }
    }

    for (let key in servicesByHost) {
      if (servicesByHost.hasOwnProperty(key)) {
        $scope.servicesByHost.push({
          host: key,
          services: servicesByHost[key]
        })
      }
    }

    return results
  })

  $scope.$watch('view', function (newVal, oldVal) {
    if (newVal === oldVal) {
      return
    }

    $state.go('app.autostartservicehosts', { view: $scope.view }, {notify: false})
  })

  $scope.$root.topRightMenu.setItems([])
}])
