window.discoApp.controller('DbEditCtrl', ['$scope', '$mdDialog', 'db', 'DbTypes', function ($scope, $mdDialog, db, DbTypes) {
  $scope.db = db

  $scope.types = DbTypes

  $scope.delete = function () {
    $mdDialog.hide({
      action: 'Delete',
      db
    })
  }

  $scope.close = function () {
    $mdDialog.cancel()
  }

  $scope.submit = function () {
    $mdDialog.hide({
      action: 'Edit',
      db
    })
  }
}])
