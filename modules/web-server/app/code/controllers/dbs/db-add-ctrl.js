window.discoApp.controller('DbAddCtrl', ['$scope', '$mdDialog', 'DbTypes', function ($scope, $mdDialog, DbTypes) {
  $scope.types = DbTypes

  $scope.db = {
    _id: '',
    type: $scope.types[0].key,
    uri: ''
  }

  $scope.delete = function () {
    $mdDialog.hide({
      action: 'Delete',
      db
    })
  }

  $scope.close = function () {
    $mdDialog.cancel()
  }

  $scope.submit = function () {
    $mdDialog.hide({
      action: 'Edit',
      db
    })
  }
}])
