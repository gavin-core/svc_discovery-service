window.discoApp.controller('DbsCtrl', ['$scope', '$mdDialog', 'dbs', '$window', function ($scope, $mdDialog, dbs, $window) {
  $scope.dbs = dbs

  $scope.showAdd = ev => {
    $mdDialog.show({
      controller: 'DbAddCtrl',
      templateUrl: 'views/dbs/add.html',
      targetEvent: ev,
      clickOutsideToClose: false,
      parent: window.angular.element(document.body),
      fullscreen: true
    }).then(function successCb (res) {
      switch (res.action) {
        case 'Add':
          return _addDb(res.db)
        default:
          throw new Error(`action '${res.action}' has not been catered for, fix!`)
      }
    }, function errCb () {
    })
  }

  $scope.showEdit = (ev, db) => {
    $mdDialog.show({
      controller: 'DbEditCtrl',
      locals: {
        db
      },
      templateUrl: 'views/dbs/edit.html',
      targetEvent: ev,
      clickOutsideToClose: false,
      parent: window.angular.element(document.body),
      fullscreen: true
    }).then(function successCb (res) {
      switch (res.action) {
        case 'Delete':
          return _deleteDb(res.db)
        case 'Edit':
          return _editDb(res.db)
        default:
          throw new Error(`action '${res.action}' has not been catered for, fix!`)
      }
    }, function errCb () {
    })
  }

  const _addDb = () => {
    $widow.alert('add')
  }

  const _editDb = () => {
    $window.alert('edit')
  }

  const _deleteDb = () => {
    $window.alert('delete')
  }

  $scope.$root.topRightMenu.setItems([{
    title: 'Add Database',
    fn: $scope.showAdd
  }])
}])
