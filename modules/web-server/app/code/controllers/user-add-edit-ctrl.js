window.discoApp.controller('UserAddEditCtrl', ['$scope', '$mdDialog', 'action', 'user', function ($scope, $mdDialog, action, user) {
  $scope.action = action
  $scope.user = user || {
    username: '',
    password: ''
  }

  const close = () => {
    $mdDialog.cancel()
  }

  const submit = () => {
    if (!$scope.userAddEditForm.$valid) {
      return
    }

    $mdDialog.hide({
      action: $scope.action,
      user: $scope.user
    })
  }

  $scope.ui = {
    title: action.toLowerCase() === 'Add' ? 'Add' : 'Edit',
    input: {
      username: {
        value: $scope.user.username,
        required: true
      },
      password: {
        value: $scope.user.password,
        required: true,
        minlength: 6
      }
    },
    buttons: {
      cancel: {
        title: 'Cancel',
        fn: close
      },
      submit: {
        title: action.toLowerCase() === 'Add' ? 'Add' : 'Save',
        fn: submit
      }
    }
  }
}])
