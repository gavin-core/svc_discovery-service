window.discoApp.controller('AppCtrl', ['$rootScope', '$scope', '$mdSidenav', '$mdMedia', function ($rootScope, $scope, $mdSidenav, $mdMedia) {
  $scope.toggleLeft = function () {
    $mdSidenav('left').toggle()
  }

  $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
    $rootScope.contentTitle = toState.title || 'View'

    if ($mdSidenav('left', true)) {
      $mdSidenav('left', true).close()
    }
  })
}])
