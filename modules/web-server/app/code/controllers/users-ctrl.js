window.discoApp.controller('UsersCtrl', ['$scope', '$mdDialog', '$window', 'users', 'UserCreator', function ($scope, $mdDialog, $window, users, UserCreator) {
  $scope.users = users

  const showAdd = (ev) => {
    const locals = {
      action: 'Add',
      user: null
    }

    show(ev, locals)
  }

  const show = (ev, locals) => {
    $mdDialog.show({
      controller: 'UserAddEditCtrl',
      locals,
      templateUrl: 'views/users/add-edit.html',
      targetEvent: ev,
      clickOutsideToClose: false,
      parent: window.angular.element(document.body),
      fullscreen: true
    }).then(function successCb (res) {
      switch(res.action.toLowerCase()) {
        case 'add':
          return addUser(res.user)
        case 'edit':
          return editUser(res.user)
        case 'delete':
          return deleteUser(res.user)
        default:
          throw new Error(`Action '${res.action}' has not been catered for. FIX!!`)
      }
    }, function errCb () {})
  }

  const addUser = user => {
    UserCreator(user)
      .$promise
      .then(user => {
        $scope.users.push(user)
        sortUsers()
      }, err => {
        $window.alert(err)
      })
  }

  const editUser = user => {
    $window.alert('TODO Edit User')
  }

  const deleteUser = user => {
    $window.alert('TODO Delete User')
  }

  const sortUsers = () => {
    $scope.users.sort((a, b) => a.username.toLowerCase() > b.username.toLowerCase())
  }

  $scope.$root.topRightMenu.setItems([{
    title: 'Add User',
    fn: showAdd
  }])
}])
