window.discoApp.filter('duration', [function () {
  return function (value) {
    if (!+value) {
      return ''
    }

    return parseInt(value / (1000 * 60), 10)
  }
}])
