window.discoApp.factory('AuthInterceptor', ['$injector', '$timeout', function authInterceptor ($injector, $timeout) {
  let AuthService

  return {
    request: config => {
      if (config.url.indexOf('/api/') < 0 ||
        config.url.indexOf('/authorize') >= 0 ||
        config.url.indexOf('/logout') >= 0) {
        return config
      }

      if (!AuthService) {
        AuthService = $injector.get('AuthService')
      }

      return AuthService.getToken(true)
        .then(token => {
          config.headers.Authorization = 'Bearer ' + token
          return config
        })
    },
    response: res => {
      if (res.config.url.indexOf('/authorize') >= 0 && res.data.token) {
        AuthService.saveToken(res.data.token)
      }

      return res
    }
  }
}])
