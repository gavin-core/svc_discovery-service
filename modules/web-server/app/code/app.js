const angular = window.angular

window.discoApp = angular.module('discoApp', ['ui.router', 'ngCookies', 'ngAnimate', 'ngMaterial', 'ngResource', 'angular.filter'])
  .config(['$mdThemingProvider', '$mdIconProvider', '$stateProvider', '$urlRouterProvider', '$httpProvider', '$resourceProvider', function ($mdThemingProvider, $mdIconProvider, $stateProvider, $urlRouterProvider, $httpProvider, $resourceProvider) {
    $resourceProvider.defaults.actions = {
      create: { method: 'POST' },
      get: { method: 'GET' },
      query: { method: 'GET', isArray:true },
      update: { method: 'PUT' },
      delete: { method: 'DELETE' }
    }

    $mdThemingProvider.definePalette('default', {
      '50': '111',
      '100': '044',
      '200': '044',
      '300': '044',
      '400': '044',
      '500': '464',
      '600': '686',
      '700': '111',
      '800': '222',
      '900': '044',
      'A100': '000',
      'A200': '000',
      'A400': '000',
      'A700': '000',
      // whether, by default, text (contrast)
      // on this palette should be dark or light
      'contrastDefaultColor': 'light',
      // hues which contrast should be 'dark' by default
      'contrastDarkColors': ['50', '100', '200', '300', '400', 'A100']
    })

    $mdThemingProvider.theme('success')
    $mdThemingProvider.theme('warning')
    $mdThemingProvider.theme('error')
    $mdThemingProvider.theme('default')
      .dark()
      .primaryPalette('default')
      .accentPalette('orange')

    $mdIconProvider.defaultFontSet('fa')

    $httpProvider.interceptors.push('AuthInterceptor')

    routes($stateProvider, $urlRouterProvider)
  }])
  .run(['$rootScope', '$mdSidenav', '$mdMenu', 'AuthService', function ($rootScope, $mdSidenav, $mdMenu, AuthService) {
    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
      $rootScope.contentTitle = toState.title || 'View'
    })

    $rootScope.$mdMenu = $mdMenu

    $rootScope.topRightMenu = []

    $rootScope.topRightMenu.setItems = items => {
      $rootScope.topRightMenu.splice(0, $rootScope.topRightMenu.length)

      ;(items || []).forEach(item => {
        $rootScope.topRightMenu.push(item)
      })

      $rootScope.topRightMenu.push({
        title: 'Log Out',
        fn: () => AuthService.logout()
      })
    }

    $rootScope.topRightMenu.setItems()
  }])

let routes = function ($stateProvider, $urlRouterProvider) {
  $stateProvider.state('app', {
    url: '/app',
    abstract: true,
    controller: 'AppCtrl',
    templateUrl: 'views/in.html'
  })

  $stateProvider.state('app.autostartservicehosts', {
    url: '/autostartservicehosts?view',
    title: 'Auto Start Service Hosts',
    views: {
      'main@app': {
        controller: 'AutoStartServiceHostsCtrl',
        templateUrl: 'views/auto-start-service-hosts/index.html',
        resolve: {
          view: ['$stateParams', function ($stateParams) {
            return $stateParams.view
          }],
          autoStartServiceHosts: ['AutoStartServiceHostsLoader', '$stateParams', function (AutoStartServiceHostsLoader, $stateParams) {
            return AutoStartServiceHostsLoader({})
          }]
        }
      }
    }
  })

  $stateProvider.state('app.autostartservices', {
    url: '/autostartservices',
    title: 'Auto Start Services',
    views: {
      'main@app': {
        controller: 'AutoStartServicesCtrl',
        templateUrl: 'views/auto-start-services/index.html'
      }
    },
    resolve: {
      autoStartServices: ['AutoStartServicesLoader', '$stateParams', function (AutoStartServicesServicesLoader, $stateParams) {
        return AutoStartServicesServicesLoader({})
      }]
    }
  })

  $stateProvider.state('app.dbs', {
    url: '/dbs',
    title: 'Databases',
    views: {
      'main@app': {
        controller: 'DbsCtrl',
        templateUrl: 'views/dbs/index.html'
      }
    },
    resolve: {
      dbs: ['DbsLoader', '$stateParams', function (DbsLoader, $stateParams) {
        return DbsLoader({})
      }]
    }
  })

  $stateProvider.state('app.services', {
    url: '/services?view',
    title: 'Services',
    views: {
      'main@app': {
        controller: 'ServicesCtrl',
        templateUrl: 'views/services/index.html',
        redirectTo: ['$stateParams', function ($stateParams) {
          switch ($stateParams.view.toLowerCase()) {
            case 'all':
            case 'hosts':
          }
          // TODO
        }],
        resolve: {
          view: ['$stateParams', function ($stateParams) {
            return $stateParams.view
          }],
          services: ['ServicesLoader', '$stateParams', function (ServicesLoader, $stateParams) {
            return ServicesLoader({ type: 2 })
          }]
        }
      }
    }
  })

  $stateProvider.state('app.users', {
    url: '/users',
    title: 'Users',
    views: {
      'main@app': {
        controller: 'UsersCtrl',
        templateUrl: 'views/users/index.html',
        resolve: {
          users: ['UsersLoader', '$stateParams', function (UsersLoader, $stateParams) {
            return UsersLoader({})
          }]
        }
      }
    }
  })

  $urlRouterProvider.otherwise('/app/services')

  routes = angular.noop
}
