require('./prototypes')

const readline = require('readline')
const core = require('node-core')
const modules = require('./modules')
const middleware = require('./middleware')

const Crypto = middleware.Crypto
const db = middleware.db
const config = core.config
const logger = core.logger

const ApiServer = modules.ApiServer
let DiscoveryHost = modules.DiscoveryHost
const WebServer = modules.WebServer

/**/
process.on('uncaughtException', err => {
  console.log('uncaughtException')
  console.error(err.stack)
})
/**/

const _onExit = () => {
  logger.verbose('asked to terminate')

  if (process.askedToTerminate) {
    return
  }

  process.askedToTerminate = true

  _tryKill()
  setInterval(_tryKill, 5000)
}

const _tryKill = () => {
  if (!DiscoveryHost || !DiscoveryHost.terminate) {
    return process.exit()
  }

  DiscoveryHost.terminate(() => {
    process.exit()
  })
}

process.on('SIGINT', _onExit)
    .on('SIGTERM', _onExit)

let rl
if (process.platform === 'win32') {
  rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  })

  rl.on('SIGINT', _onExit)
}

const options = {
  preferredIPMask: config.preferredIPMask,
  udp: {
    port: config.discovery.udpPort
  },
  priority: config.discovery.priority
}

const masterSecret = config.masterSecret

if (!masterSecret) {
  throw new Error('No \'masterSecret\' found, please specify')
}

const crypto = new Crypto('sha256', masterSecret, 'hex')

const cookieConfig = {
  name: config.cookie && config.cookie.name,
  domain: config.cookie && config.cookie.domain,
  path: (config.cookie && config.cookie.path) || '/',
  ttl: (config.cookie && config.cookie.ttl) || 10,
  secret: config.cookie && config.cookie.secret
}

const jwtConfig = {
  secret: config.jwt && config.jwt.secret,
  ttl: (config.jwt && config.jwt.ttl) || 10
}

;[
  function ensureUserExists(next, data) {
    db.getDb().users.count((err, count) => {
      if (err) {
        return next(err)
      }

      data.userExists = !!count
      next()
    })
  },
  function getUsername(next, data) {
    if (data.userExists) {
      return next()
    }

    writeToTerminal('Username:')
    readFromTerminal((err, username) => {
      if (err) {
        return next(err)
      }


      data.username = username
      next()
    })
  },
  function getPassword(next, data) {
    if (data.userExists) {
      return next()
    }

    writeToTerminal('Password:')
    readFromTerminal((err, password) => {
      if (err) {
        return next(err)
      }

      console.log('password', password)
      data.password = password
      next()
    })
  },
  function createUser(next, data) {
    if (data.userExists) {
      return next()
    }

    const user = {
      username: data.username,
      password: data.crypto.encrypt(data.password)
    }

    db.getDb().users.insert(user, (err, resp) => {
      if (err) {
        return next(err)
      }

      next()
    })
  },
  function createDiscoveryHost(next, data) {
    DiscoveryHost = DiscoveryHost(options)
    next()
  },
  function createApiServer(next, data) {
    if (config.api && config.api.disable) {
      return next()
    }

    data.apiServer = new ApiServer(DiscoveryHost, cookieConfig, jwtConfig, data.crypto)
    next()
  },
  function createWebServer(next, data) {
    if (config.web && config.web.disable) {
      return next()
    }

    data.webServer = new WebServer(DiscoveryHost, cookieConfig, jwtConfig, data.crypto)
    next()
  }
].callInSequence((err, data) => {
  if (err) {
    throw err
  }
}, {
  crypto
})

const writeToTerminal = text => {
  console.log(text.toString().white)
}

const readFromTerminal = cb => {
  if (rl) {
    const rlListener = line => {
      rl.removeListener('line', rlListener)
      return cb(null, line.toString().trim())
    }

    return rl.on('line', rlListener)
  }

  const listener = () => {
    const chunk = process.stdin.read()
    if (chunk !== null) {
      process.stdin.removeListener('readable', listener)
      return cb(null, chunk.toString().trim())
    }
  }

  process.stdin.on('readable', listener)
}
