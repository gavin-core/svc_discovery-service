const mongo = require('mongojs')
global.ObjectID = mongo.ObjectId

if (!('toObjectID' in String.prototype)) {
  String.prototype.toObjectID = function () {
    try {
      return mongo.ObjectId(this)
    } catch (e) {
      return null
    }
  }
}

if (!('toObjectID' in mongo.ObjectID.prototype)) {
  mongo.ObjectID.prototype.toObjectID = function () {
    return this
  }
}
