const core = require('node-core')
const mongojs = require('mongojs')

let db
let connUri = core.config.discoveryBackendUri

if (!connUri) {
  throw new Error('No backend database specified in the config file of the discovery service... A conn string is required for the service to work')
}

const _initDb = cb => {
  db = mongojs(connUri, [
    'auto_start_services',
    'auto_start_service_hosts',
    'dbs',
    'services',
    'users',
    'vars'
  ])

  db.on('connect', () => {
    // connect only happens when first request is made, not immediately
  })

  ;(cb || (() => {}))()
}

_initDb()
module.exports = {
  setDbUri: (uri, cb) => {
    cb = cb || (() => {})

    if (uri === connUri) {
      return
    }

    connUri = uri
    _initDb(cb)
  },
  getDb: () => db
}
