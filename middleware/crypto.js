const crypto = require('crypto')

module.exports = function (algorithm, secret, type) {
  const _this = this
  const _secret = secret
  const _type = type

  _this.encrypt = (value) => crypto.createHmac(algorithm, _secret)
    .update(value)
    .digest(_type)

  return _this
}
